import unittest
from selenium import webdriver
from  pages.LoginTestPage import LoginTestPage

class LoginTest(unittest.TestCase):
    driverPath = "F:\python_automationTesting\drivers\chromedriver.exe"
    baseURL = "https://www.cleartrip.com/"
    userName="tusharsinhaa95@gmail.com"
    password="Password@1"

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome(executable_path=cls.driverPath)
        cls.driver.get(cls.baseURL)
        cls.driver.implicitly_wait(5)
        cls.driver.maximize_window()

    def test_login(self):
        self.lp=LoginTestPage(self.driver)
        self.lp.clickSignIn()
        self.lp.clickSignBtn()
        self.lp.switchFrame()
        self.lp.setUserName(self.userName)
        self.lp.setPassword(self.password)
        self.lp.submit()

    @classmethod
    def tearDownClass(cls):
        cls.driver.close()
