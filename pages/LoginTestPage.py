class LoginTestPage:
#Locators
    signIn_dropdown="//span[text()='Your trips']"
    signIn_btn ="//input[@title='Sign In']"
    userName_txtbox="//input[@name='email']"
    password_txtbox="//input[@title='Your account password']"
    submit_btn="//button[@id='signInButton']"
    iframe="modal_window"

#constructor
    def __init__(self,driver):
        self.driver=driver

    def clickSignIn(self):
        self.driver.find_element_by_xpath(self.signIn_dropdown).click()
    def clickSignBtn(self):
        self.driver.find_element_by_xpath(self.signIn_btn).click()
    def switchFrame(self):
        iframe=self.driver.find_element_by_id("modal_window")
        self.driver.switch_to.frame(iframe)
    def setUserName(self,user):
        self.driver.find_element_by_xpath(self.userName_txtbox).send_keys(user)
    def setPassword(self,password):
        self.driver.find_element_by_xpath(self.password_txtbox).send_keys(password)
    def submit(self):
        self.driver.find_element_by_xpath(self.submit_btn)